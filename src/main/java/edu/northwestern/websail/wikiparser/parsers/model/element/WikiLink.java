package edu.northwestern.websail.wikiparser.parsers.model.element;

import edu.northwestern.websail.wikiparser.parsers.model.WikiTitle;

public class WikiLink extends WikiElement {

    private WikiTitle target;
    private String surface;
    private LinkType linkType;

    public WikiLink() {
        super();
    }

    public WikiLink(int start, int end, WikiPageLocationType locType, String surface, WikiTitle target,
                    LinkType type) {
        super(start, end, locType);
        this.setTarget(target);
        this.surface = surface;
        this.linkType = type;
    }

    public WikiLink(int start, int end, WikiPageLocationType locType, String surface, WikiTitle target) {
        super(start, end, locType);
        this.setTarget(target);
        this.surface = surface;
        this.linkType = LinkType.UNSET;
    }

    public WikiLink(int start, int end, WikiPageLocationType locType, boolean isInTemplate, String surface, WikiTitle target) {
        super(start, end, locType, isInTemplate);
        this.setTarget(target);
        this.surface = surface;
    }

    public WikiTitle getTarget() {
        return target;
    }

    public void setTarget(WikiTitle target) {
        this.target = target;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public LinkType getLinkType() {
        return linkType;
    }

    public void setLinkType(LinkType linkType) {
        this.linkType = linkType;
    }

    @Override
    public String toString() {
        return this.surface + "->" + target;
    }

}
