package edu.northwestern.websail.wikiparser.parsers.wikitextparser.utils;

import com.google.gson.Gson;
import edu.northwestern.websail.wikiparser.parsers.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.parsers.model.element.*;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ExtractedPageUtils {

	public ExtractedPageUtils() {
		
	}

	public static void printPage(WikiExtractedPage page){
		boolean print  = false;
		if(!print) return;
		System.out.println(page.getTitle());
		System.out.println("Plain Text:");
		System.out.println(page.getPlainText());
		System.out.println("Section Headers:");
		for(WikiSection section: page.getSections()){
			System.out.print(section.getSectionNum()+".");
			System.out.println(page.getPlainText().substring(section.getOffset(), section.getEndHeadingOffset()));
		}
		System.out.println("Links:");
		for(WikiLink internalLink : page.getInternalLinks()){
			//System.out.println(text.substring(internalLink.getOffset(), internalLink.getEndOffset()));
			System.out.println(internalLink + "@" + internalLink.getLocType());
		}
		System.out.println("Category Links:");
		for(WikiLink internalLink : page.getCategoryLinks()){
			System.out.println(internalLink + "@" + internalLink.getLocType());
		}
		System.out.println("InterWiki Links:");
		for(WikiLink internalLink : page.getInterWikiLinks()){
			System.out.println(internalLink + "@" + internalLink.getLocType());
		}
		System.out.println("Paragraphs:");
		for(WikiParagraph p : page.getParagraphs()){
			System.out.println(p.getParagraphNum() + " " + p.getLocType());
			System.out.println(page.getPlainText().substring(p.getOffset(), p.getEndOffset()));
		}
		System.out.println("Tables:");
		for(WikiHTMLElement t : page.getTableHTMLs()){
			System.out.println("@"+t.getLocType());
			System.out.println(t.getHtml());
		}
	
			
		
	}

	public static WikiExtractedPage recoverOverviewParagraph(WikiExtractedPage page){
		if(page.getParagraphs().size() == 0 
				|| page.getPlainText().trim().equals("")
				|| page.getSections().size() == 0) return page;
		if(page.getParagraphs().get(0).getLocType() == WikiPageLocationType.OVERVIEW) return page;
		String firstP = page.getPlainText().substring(0, page.getSections().get(0).getOffset());
		WikiParagraph p = new WikiParagraph(0, firstP.length(), 0, WikiPageLocationType.OVERVIEW);
		for(WikiParagraph eP : page.getParagraphs()){
			eP.setParagraphNum(eP.getParagraphNum()+1);
		}
		page.getParagraphs().add(0, p);
		return page;
	}
	
	public static void serialize(WikiExtractedPage page, String directory) throws FileNotFoundException, IOException{
		Gson gson = new Gson();
		PrintStream out = new PrintStream(new GZIPOutputStream(
    			new FileOutputStream(directory+"/"+page.getTitle().getId())));
		gson.toJson(page, out);
		out.close();
	}
	
	public static WikiExtractedPage deserialize(String directory) throws FileNotFoundException, IOException {
		Gson gson = new Gson();
		InputStreamReader jsonReader = new InputStreamReader(
				new GZIPInputStream(
						new FileInputStream(directory)));
		WikiExtractedPage page = gson.fromJson(jsonReader, WikiExtractedPage.class);
		return page;
	}
}
