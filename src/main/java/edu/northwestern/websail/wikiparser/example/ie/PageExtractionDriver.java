package edu.northwestern.websail.wikiparser.example.ie;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import edu.northwestern.websail.wikiparser.dumpparser.parser.Executor;
import edu.northwestern.websail.wikiparser.example.io.ErrorPageFileExporter;
import edu.northwestern.websail.wikiparser.example.io.MongoDBPageExporter;
import edu.northwestern.websail.wikiparser.example.io.PageExtractionResultDelegate;
import edu.northwestern.websail.wikiparser.example.template.PageTemplateDriver;
import edu.northwestern.websail.wikiparser.example.template.TemplateAbstractCollector;
import edu.northwestern.websail.wikiparser.parsers.wikitextparser.sweblewrapper.SwebleLocalExpansion;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Level;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PageExtractionDriver {

    public PageExtractionDriver() {
    }

    public static void main(String[] args) throws IOException {

        System.setProperty("log4j.configuration", PageExtractionDriver.class.getResource("/log4j.properties")
                .toString());

        org.apache.log4j.Logger.getLogger(HttpClient.class).setLevel(Level.ERROR);
        org.apache.log4j.Logger.getLogger(org.apache.commons.httpclient.HttpClient.class).setLevel(Level.ERROR);
        org.apache.log4j.Logger.getLogger(org.apache.commons.httpclient.HttpClient.class).setLevel(Level.ERROR);
        org.apache.log4j.Logger.getLogger(sun.net.www.http.HttpClient.class).setLevel(Level.ERROR);

        int numThreads = 5;
        int numInFiles = 5;
        String dumpFileDirectory = args[0];
        boolean compressed = Boolean.valueOf(args[1]);
        String language = args[2];
        String templateFile = args[3];
        boolean saveOutput = Boolean.valueOf(args[4]);

        Properties mongoConfig = new Properties();
        mongoConfig.load(new FileInputStream("./config/mongo.properties"));

        PageExtractionResultDelegate exporter = null;
        MongoClient mongoClient = null;
        if (saveOutput) {
            DB db = null;
            try {
                mongoClient = new MongoClient(mongoConfig.getProperty("hostName"));
                db = mongoClient.getDB(mongoConfig.getProperty("dbName"));
                if (!db.authenticate(mongoConfig.getProperty("username"), mongoConfig.getProperty("password")
                        .toCharArray())) {
                    System.out.println("username or password is not correct.");
                    mongoClient.close();
                    return;
                }
            } catch (UnknownHostException e1) {
                System.out.println("Host DB error");
                e1.printStackTrace();
                return;
            }
            System.out.println("Connecting to mongo success." + mongoClient);
            exporter = new MongoDBPageExporter("error_pages/" + language, db);
        } else {
            exporter = new ErrorPageFileExporter("error_pages/" + language);
        }
        TemplateAbstractCollector collector = new PageTemplateDriver.HashMapTemplateCollector();
        PageTemplateDriver.APITemplateCollector backupCollector = new PageTemplateDriver.APITemplateCollector();
        try {
            System.out.println("Load templates >:[");
            collector = collector.deserialize(templateFile);
            System.out.println("Template Size: " + collector.numTemplate());
        } catch (Exception e) {
            System.out.println("fail to load template.");
            e.printStackTrace();
        }

        SwebleLocalExpansion expansion = new SwebleLocalExpansion(collector, language);
//        expansion.setBackupCollector(backupCollector);
        // For some cases where gettig template expansion from API is better.

        File folder = new File(dumpFileDirectory);

        ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(numInFiles * 2);
        try {
            ThreadPoolExecutor exec = new ThreadPoolExecutor(numInFiles, numInFiles, 5L,
                    TimeUnit.MINUTES, queue, new ThreadPoolExecutor.CallerRunsPolicy());

            for (File file : folder.listFiles()) {
                if (file.getName().charAt(0) == '.') continue;
                //Executor.readFile(file.getAbsolutePath(), numThreads, new PageExtractionWorker(language, expansion), compressed);
                PageExtractionWorker pageWorker = new PageExtractionWorker(language, expansion);
                pageWorker.setExtractionDelegate(exporter);
                DumpFileWorker worker = new DumpFileWorker(file, compressed, numThreads, pageWorker);
                exec.submit(worker);
            }

            exec.shutdown();
            try {
                exec.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                System.out.println("Failed to terminate");
                e.printStackTrace();
            }

            if (mongoClient != null) mongoClient.close();

        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage() + ":" + e.getCause());
            e.printStackTrace();
        }


    }

    private static class DumpFileWorker implements Runnable {
        File file;
        boolean compressed;
        int numThreads;
        PageExtractionWorker pageWorker;

        public DumpFileWorker(File file, boolean compressed, int numThreads, PageExtractionWorker pageWorker) {
            this.file = file;
            this.compressed = compressed;
            this.numThreads = numThreads;
            this.pageWorker = pageWorker;
        }

        @Override
        public void run() {
            Executor.readFile(file.getAbsolutePath(), numThreads, pageWorker, compressed);
        }

    }


}
