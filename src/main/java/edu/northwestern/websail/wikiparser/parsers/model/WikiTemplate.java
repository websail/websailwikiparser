package edu.northwestern.websail.wikiparser.parsers.model;

import org.jongo.marshall.jackson.oid.ObjectId;

import java.io.Serializable;

public class WikiTemplate implements Serializable {
	@ObjectId
	private String key;//for jongo
	private static final long serialVersionUID = -7468053472326973114L;
	private String fullTitleName;
	private int id;
	private String wikiText;
	private String redirectedTemplateName;
	private boolean redirecting;
	public WikiTemplate(){}
	public WikiTemplate(int id, String fullTitleName, String wikiText) {
		this.id = id;
		this.fullTitleName = fullTitleName;
		this.wikiText = wikiText;
	}

	public String getFullTitleName() {
		return fullTitleName;
	}

	public int getId() {
		return id;
	}

	public String getWikiText() {
		return wikiText;
	}

	public void setFullTitleName(String fullTitleName) {
		this.fullTitleName = fullTitleName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setWikiText(String wikiText) {
		this.wikiText = wikiText;
	}

	public String getRedirectedTemplateName() {
		return redirectedTemplateName;
	}

	public boolean isRedirecting() {
		return redirecting;
	}

	public void setRedirectedTemplateName(String redirectedTemplateName) {
		this.redirectedTemplateName = redirectedTemplateName;
	}

	public void setRedirecting(boolean redirecting) {
		this.redirecting = redirecting;
	}

	public String getKey() {
		return key;
	}

	public void set_id(String _id) {
		this.key = _id;
	}
	
	

}
