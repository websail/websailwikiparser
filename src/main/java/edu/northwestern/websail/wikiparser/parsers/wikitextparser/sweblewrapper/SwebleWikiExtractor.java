package edu.northwestern.websail.wikiparser.parsers.wikitextparser.sweblewrapper;

/**
 * @author NorThanapon
 * Wrapper Class of Sweble Engine http://sweble.org 
 * Paper: http://dl.acm.org/citation.cfm?id=2491057
 */

import com.mongodb.DB;
import com.mongodb.MongoClient;
import de.fau.cs.osr.utils.FmtNotYetImplementedError;
import edu.northwestern.websail.wikiparser.example.template.PageTemplateDriver;
import edu.northwestern.websail.wikiparser.example.template.TemplateAbstractCollector;
import edu.northwestern.websail.wikiparser.parsers.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.parsers.model.factory.WikiTitleFactory;
import edu.northwestern.websail.wikiparser.wikipediaApiWrapper.WikiApiResponsePage;
import edu.northwestern.websail.wikiparser.wikipediaApiWrapper.WpAPI;
import org.apache.commons.io.FileUtils;
import org.jongo.Jongo;
import org.sweble.wikitext.engine.*;
import org.sweble.wikitext.engine.config.WikiConfig;
import org.sweble.wikitext.engine.config.WikiConfigImpl;
import org.sweble.wikitext.engine.nodes.EngProcessedPage;
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp;
import org.sweble.wikitext.parser.parser.LinkTargetException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class SwebleWikiExtractor {

    private String language;
    private WikiConfig config;
    private ExpansionCallback expansionCallback; // mainly for template expansion

    public SwebleWikiExtractor(String language, WikiConfig config) {
        this.language = language;
        this.config = config;
        expansionCallback = null;
    }

    public SwebleWikiExtractor(String language, WikiConfig config, ExpansionCallback expansionCallback) {
        this.config = config;
        this.language = language;
        this.expansionCallback = expansionCallback;
    }

    /**
     * Parse WikiText into WikiExtractedPage
     *
     * @param pageLocalId
     * @param fullPageName
     * @param wikiText
     * @return WikiExtractedPage
     * @throws LinkTargetException
     * @throws EngineException
     */
    public WikiExtractedPage parse(int pageLocalId, String fullPageName, String wikiText) throws LinkTargetException, EngineException, IOException, FmtNotYetImplementedError {
        // Instantiate a compiler for wiki pages

        org.apache.log4j.Logger.getLogger(WtEngineImpl.class).setLevel(org.apache.log4j.Level.OFF);

        wikiText = SwebleUtils.cleanWikiText(wikiText);
        WtEngineImpl engine = new WtEngineImpl(config);
        // Retrieve a page
        PageTitle pageTitle = PageTitle.make(config, fullPageName);
        PageId pageId = new PageId(pageTitle, pageLocalId);
        WikiTitleFactory titleFactory = (new WikiTitleFactory()).language(language);
        WikiExtractedPage wikiPage = new WikiExtractedPage(titleFactory.id(pageLocalId).title(fullPageName).produce());
        SweblePlainTextProcessor sp = new SweblePlainTextProcessor(config, wikiPage);

        sp.setTitleFactory(titleFactory);
        EngProcessedPage cp = engine.postprocess(pageId, wikiText, expansionCallback);
        wikiPage = (WikiExtractedPage) sp.go(cp.getPage());

        return wikiPage;
    }

    public void setExpansionCallback(ExpansionCallback expansionCallback) {
        this.expansionCallback = expansionCallback;
    }

    public static void main(String args[]) throws IOException, LinkTargetException, EngineException {

        TemplateAbstractCollector collector = new PageTemplateDriver.HashMapTemplateCollector();
        try {
            collector = collector.deserialize("/Users/csbhagav/Projects/Data/parser/en_templates.map");
        } catch (Exception e) {
            System.out.println("fail to load template.");
            e.printStackTrace();
        }

        SwebleLocalExpansion expansion = new SwebleLocalExpansion(collector, "en");
//        expansion.setBackupCollector(new PageTemplateDriver.APITemplateCollector());
        WikiConfigImpl config = DefaultConfigEnWp.generate();

        List<String> pgIds = FileUtils.readLines(new File("data/testPageIds.txt"));

        Properties mongoConfig = new Properties();
        mongoConfig.load(new FileInputStream("./config/mongo.properties"));

        MongoClient mongoClient = new MongoClient(mongoConfig.getProperty("hostName"));
        DB db = mongoClient.getDB(mongoConfig.getProperty("dbName"));
        if (!db.authenticate(mongoConfig.getProperty("username"), mongoConfig.getProperty("password")
                .toCharArray())) {
            System.out.println("username or password is not correct.");
            mongoClient.close();
            return;
        }
        Jongo j = new Jongo(db);

        for (int i = 0; i < pgIds.size(); i++) {
            Integer pgId = Integer.valueOf(pgIds.get(i));
            WpAPI wp = new WpAPI();
            WikiApiResponsePage p = wp.getWikiTextForPage(pgId);
            System.out.println(p.getTitle());
            SwebleWikiExtractor se = new SwebleWikiExtractor("en", config, expansion);
            WikiExtractedPage page = se.parse(p.getPgId(), p.getTitle().replaceAll(" ", "_"), p.getWikiText());

//            WikiExtractedPage page = se.parse(185052, "Australia_Part", FileUtils.readFileToString(new File
//                    ("data/test.wikitext")));

            j.getCollection("page").save(page);
        }
    }
}

