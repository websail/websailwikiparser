package edu.northwestern.websail.wikiparser.dumpparser.dao;

/**
 * @author csbhagav
 * 
 *         This class is a holds Contributor Data from the node "<contributor>"
 *         in the Wikipedia XML dump
 * 
 */
public class WikiContributor {

	String userName;
	String id;

	public WikiContributor(String userName, String id) {
		super();
		this.userName = userName;
		this.id = id;
	}

	public WikiContributor() {
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String toString() {
		String tabLevel = "\t\t";
		String retStr = tabLevel + "Username: " + userName + "\n";
		retStr += tabLevel + "id: " + id + "\n";
		return retStr;
	}

}
