package edu.northwestern.websail.wikiparser.example.io;

import edu.northwestern.websail.wikiparser.parsers.model.WikiExtractedPage;

/**
 * Created by csbhagav on 1/12/14.
 */
public class PageExtractionDelegateSequence implements PageExtractionResultDelegate {

    private Iterable<PageExtractionResultDelegate> sequence;

    public PageExtractionDelegateSequence(Iterable<PageExtractionResultDelegate> sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean doneExtractingPage(WikiExtractedPage page) {
        boolean success = true;
        for (PageExtractionResultDelegate delegate : sequence) {
            success = success && delegate.doneExtractingPage(page);
        }
        return success;
    }

    @Override
    public boolean errorExtractingPage(int titleId, String titleName,
                                       String wikiText) {
        boolean success = true;
        for (PageExtractionResultDelegate delegate : sequence) {
            success = success && delegate.errorExtractingPage(titleId, titleName, wikiText);
        }
        return success;
    }

}
