package edu.northwestern.websail.wikiparser.example.io;

import edu.northwestern.websail.wikiparser.parsers.model.WikiExtractedPage;

/**
 * Created by csbhagav on 1/12/14.
 */
public interface PageExtractionResultDelegate {
    public boolean doneExtractingPage(WikiExtractedPage page);

    public boolean errorExtractingPage(int titleId, String titleName, String wikiText);
}
