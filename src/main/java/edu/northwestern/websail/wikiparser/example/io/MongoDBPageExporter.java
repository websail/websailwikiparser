package edu.northwestern.websail.wikiparser.example.io;

import com.mongodb.DB;
import com.mongodb.WriteResult;
import edu.northwestern.websail.wikiparser.parsers.model.table.WikiTable;
import edu.northwestern.websail.wikiparser.parsers.model.WikiExtractedPage;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

/**
 * Created by csbhagav on 1/12/14.
 */
public class MongoDBPageExporter extends ErrorPageFileExporter {
    private DB db;
    private Jongo jongo;
    private MongoCollection pages;
    private MongoCollection tables;
    private String collectionName = "page";
    private String tableCollectionName = "tables";

    public MongoDBPageExporter(String errorPath, DB db) {
        super(errorPath);
        this.db = db;
//        this.removePageCollection();
        this.jongo = new Jongo(db);
        this.pages = jongo.getCollection(collectionName);
        this.tables = jongo.getCollection(tableCollectionName);
    }

    @Override
    public boolean doneExtractingPage(WikiExtractedPage page) {
        WriteResult r = pages.save(page);
        return r.getN() > 0;
    }

    private void removePageCollection() {
        if (db.collectionExists(collectionName))
            db.getCollection(collectionName).drop();
    }

    public boolean doneExtractingTable(WikiTable table) {
        WriteResult r = tables.save(table);
        return r.getN() > 0;
    }
}
