package edu.northwestern.websail.wikiparser.example.titleprinter;

/**
 * @author NorThanapon
 */

import edu.northwestern.websail.wikiparser.dumpparser.parser.WikiPageAbstractWorker;

public class PageTitleWorker extends WikiPageAbstractWorker{

	private int id;
	
	
	public PageTitleWorker(int id){
		this.id = id;
	}
	
	@Override
	public void run() {
		System.out.println(id);
		System.out.println(getPage().getTitle());
	}

	@Override
	public PageTitleWorker newInstance(){
		return new PageTitleWorker(this.id+1);
	}

}
