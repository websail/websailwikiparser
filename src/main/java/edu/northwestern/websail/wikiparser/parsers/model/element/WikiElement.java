package edu.northwestern.websail.wikiparser.parsers.model.element;

public abstract class WikiElement {
	protected int offset;
	protected int endOffset;
	protected boolean isInTemplate;
	protected WikiPageLocationType locType;
	
	public WikiElement(){}
	
	public WikiElement(int start, int end, WikiPageLocationType locType, boolean isInTemplate){
		this.offset = start;
		this.endOffset = end;
		this.isInTemplate = isInTemplate;
		this.locType = locType;
	}
	
	public int getOffset() {
		return offset;
	}
	public int getEndOffset() {
		return endOffset;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public void setEndOffset(int endOffset) {
		this.endOffset = endOffset;
	}
	public WikiPageLocationType getLocType() {
		return locType;
	}

	public void setLocType(WikiPageLocationType locType) {
		this.locType = locType;
	}

	public boolean isInTemplate() {
		return isInTemplate;
	}


	public void setInTemplate(boolean isInTemplate) {
		this.isInTemplate = isInTemplate;
	}

	public WikiElement(int start, int end, WikiPageLocationType locType){
		this.offset = start;
		this.endOffset = end;
		this.locType = locType;
		this.isInTemplate = false;
	}
}
