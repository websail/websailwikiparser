package edu.northwestern.websail.wikiparser.parsers.wikitextparser.sweblewrapper;

import edu.northwestern.websail.wikiparser.example.template.TemplateAbstractCollector;
import edu.northwestern.websail.wikiparser.parsers.model.WikiTemplate;
import edu.northwestern.websail.wikiparser.parsers.wikitextparser.utils.TextUtils;
import org.sweble.wikitext.engine.*;

import java.util.HashSet;

public class SwebleLocalExpansion implements ExpansionCallback {

    TemplateAbstractCollector collector;
    private String language;
    TemplateAbstractCollector backupCollector;

    public SwebleLocalExpansion(TemplateAbstractCollector collector, String language) {
        this.collector = collector;
        this.language = language;
    }

    @Override
    public String fileUrl(PageTitle arg0, int arg1, int arg2) throws Exception {
        
        return null;
    }

    @Override
    public FullPage retrieveWikitext(ExpansionFrame frame, PageTitle pageTitle)
            throws Exception {

        if (pageTitle.getNamespace().getId() == 10) {
            if (!allowTemplate(pageTitle.toString())) return null;

            WikiTemplate template = collector.getFinalTemplate(pageTitle.toString());
            String specialExp = expandSpecialTemplates(pageTitle);
            if (specialExp != null) {
                return new FullPage(new PageId(pageTitle, template.getId()), specialExp);
            } else {
                if (useBackUpCollectorTempaltes.contains(pageTitle.toString()) || template == null && backupCollector !=
                        null) {
                    template = backupCollector.getFinalTemplate(pageTitle.toString(), pageTitle);
                }


                String text = TextUtils.replaceHTMLEntities(template.getWikiText(), null);
                FullPage p = new FullPage(new PageId(pageTitle, template.getId()), text);
                return p;
            }
        }
        return null;
    }


    public String expandSpecialTemplates(PageTitle pageTitle) {
        if (pageTitle.getTitle().equalsIgnoreCase("nts")) {
            return "{{{1}}}";
        } else if (pageTitle.getTitle().equalsIgnoreCase("left")) {
            return "";
        } else if (pageTitle.getTitle().equalsIgnoreCase("cfb_link") | pageTitle.getTitle().equalsIgnoreCase
                ("CBSB_link")) {
            return "[[{{{team}}}|{{{title}}}]]";
        } else if (pageTitle.getTitle().equalsIgnoreCase("FULLPAGENAME")) {
            return "{{{1}}}";
        } else if (pageTitle.getTitle().toLowerCase().startsWith("formatnum:")) {
            return pageTitle.getTitle().split(":", 2)[1];
        } else if (pageTitle.getTitle().toLowerCase().startsWith("fullpagename:")) {
            return pageTitle.getTitle().split(":", 2)[1];
        }else if (pageTitle.getTitle().toLowerCase().equalsIgnoreCase("ref")) {
            return "";
        } else return null;

    }

    //HACK
    public boolean allowTemplate(String templateName) {

       // return true;
		templateName = templateName.toLowerCase().replaceAll(" ", "_");
		if(this.language.equals("en")) return enAllowedTemplate.contains(templateName);
		if(this.language.equals("simple")) return simpleAllowedTemplate.contains(templateName);
		return false;
    }

    public TemplateAbstractCollector getBackupCollector() {
        return backupCollector;
    }

    public void setBackupCollector(TemplateAbstractCollector backupCollector) {
        this.backupCollector = backupCollector;
    }

    public static HashSet<String> enAllowedTemplate = new HashSet<String>();
    public static HashSet<String> simpleAllowedTemplate = new HashSet<String>();
    public static HashSet<String> useBackUpCollectorTempaltes = new HashSet<String>();

    static {
        HashSet<String> commonSet = new HashSet<String>();
        commonSet.add("template:start");
        commonSet.add("template:col-begin");
        commonSet.add("template:col-start");
        commonSet.add("template:col-break");
        commonSet.add("template:col-2");
        commonSet.add("template:col-3");
        commonSet.add("template:col-4");
        commonSet.add("template:col-5");
        commonSet.add("template:col-6");
        commonSet.add("template:col-7");
        commonSet.add("template:col-1-of-2");
        commonSet.add("template:col-2-of-2");
        commonSet.add("template:col-1-of-3");
        commonSet.add("template:col-2-of-3");
        commonSet.add("template:col-3-of-3");
        commonSet.add("template:col-1-of-4");
        commonSet.add("template:col-2-of-4");
        commonSet.add("template:col-3-of-4");
        commonSet.add("template:col-4-of-4");
        commonSet.add("template:col-1-of-5");
        commonSet.add("template:col-2-of-5");
        commonSet.add("template:col-3-of-5");
        commonSet.add("template:col-4-of-5");
        commonSet.add("template:col-5-of-5");
        commonSet.add("template:end");
        commonSet.add("template:s-end");
        commonSet.add("template:col-end");
        commonSet.add("template:end_box");
        commonSet.add("template:fb_footer");
        enAllowedTemplate.addAll(commonSet);
        simpleAllowedTemplate.add("template:end");
        simpleAllowedTemplate.add("template:s-end");

//        useBackUpCollectorTempaltes.add("cfb_link");
//        useBackUpCollectorTempaltes.add("Cbsb_link");
    }
}
