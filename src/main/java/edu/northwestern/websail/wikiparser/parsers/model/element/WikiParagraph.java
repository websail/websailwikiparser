package edu.northwestern.websail.wikiparser.parsers.model.element;

public class WikiParagraph extends WikiElement {

	protected int paragraphNum;
	public WikiParagraph(){
		super();
	}
	public WikiParagraph(int start, int end, int pNum, WikiPageLocationType locType) {
		super(start, end, locType);
		this.paragraphNum = pNum;
	}

	public WikiParagraph(int start, int end, int pNum, WikiPageLocationType locType,
			boolean isInTemplate) {
		super(start, end, locType, isInTemplate);
		this.paragraphNum = pNum;
	}
	
	public int getParagraphNum() {
		return paragraphNum;
	}

	public void setParagraphNum(int paragraphNum) {
		this.paragraphNum = paragraphNum;
	}

}
