package edu.northwestern.websail.wikiparser.parsers.model.factory;

import java.util.HashMap;

import edu.northwestern.websail.wikiparser.parsers.model.WikiTitle;

public class WikiTitleFactory {

	private int id;
	private int namespace;
	private String language;
	private String title;
	private boolean redirecting;
	private WikiTitle redirectedTitle;
	private HashMap<String, Integer> titleIdMap;
	private HashMap<String, Integer> namespaceMap;
	
	public WikiTitleFactory() {
		this.redirectedTitle = null;
		this.redirecting = false;
		this.id = 0;
		this.language = "";
		this.title = "";
		this.namespace = 0;
	}

	public WikiTitle produce(){
		WikiTitle t = new WikiTitle(language, id, title, redirecting, redirectedTitle, namespace);
		//reset temporal info
		id = 0;
		title = "";
		redirecting = false;
		redirectedTitle = null;
		return t;
	}
	
	public int id() {
		return id;
	}

	public String language() {
		return language;
	}

	public String title() {
		return title;
	}

	public boolean redirecting() {
		return redirecting;
	}

	public WikiTitle redirectedTitle() {
		return redirectedTitle;
	}

	public WikiTitleFactory id(int id) {
		this.id = id;
		return this;
	}

	public WikiTitleFactory language(String language) {
		this.language = language;
		return this;
	}

	public WikiTitleFactory title(String title) {
		this.title = title;
		if(titleIdMap!=null) {
			Integer id = titleIdMap.get(title);
			if(id!=null) return this.id(id);
		}
		return this;
	}

	public WikiTitleFactory noRedirect() {
		this.redirecting = false;
		this.redirectedTitle = null;
		return this;
	}

	public WikiTitleFactory redirect(WikiTitle redirectedTitle) {
		this.redirectedTitle = redirectedTitle;
		return this;
	}

	public int getNamespace() {
		return namespace;
	}

	public WikiTitleFactory namespace(int namespace) {
		this.namespace = namespace;
		return this;
	}
	
	public WikiTitleFactory namespace(String namespace) {
		if(namespaceMap == null) return this.namespace(-1);
		Integer ns = namespaceMap.get(namespace);
		if(ns == null) return this.namespace(-1);
		return this.namespace(ns);
	}
	

}
