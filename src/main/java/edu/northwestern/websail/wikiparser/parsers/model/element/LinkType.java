package edu.northwestern.websail.wikiparser.parsers.model.element;

/**
 * Created by csbhagav on 1/27/14.
 */
public enum LinkType {
    INTERNAL, MMUL, WEBSAIL, INTERNAL_RED, EXTERNAL, CATEGORY_LINK, UNSET
}
