package edu.northwestern.websail.wikiparser.dumpparser.parser;

import org.apache.xerces.parsers.SAXParser;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.logging.Logger;


/**
 * @author csbhagav
 *         <p/>
 *         This class provides an entry point into the library code.
 *         <p/>
 *         This class exposes a static method that parses the given file using
 *         the SAX parser.
 *         <p/>
 *         To use the library, a developer needs to call the static function
 *         startParser
 */
public class Executor {
    /**
     * @param numThreads number of threads used for processing
     * @param fileName   Wikipedia dump file to parse (pages-article*.xml)
     * @param runClass   An object of a class that extends {@link WikiPageAbstractWorker}
     */

    public static Logger logger = Logger.getLogger(Executor.class.getName());

    public static void readFile(String fileName, Integer numThreads,
                                WikiPageAbstractWorker runClass) {

        readFile(fileName, numThreads, runClass, false);
    }

    public static void readFile(String fileName, Integer numThreads,
                                WikiPageAbstractWorker runClass, Boolean isCompressed) {


        try {
            SAXParser parser = new SAXParser();

            logger.info("Reading text from file stream : " + fileName + " >:[");
            InputStream in = new FileInputStream(fileName);

            if (isCompressed) {
                BZip2CompressorInputStream bsip2stream = new BZip2CompressorInputStream(in);
                InputStreamReader isr = new InputStreamReader(bsip2stream, "UTF-8");
                InputSource source = new InputSource(isr);
                TagHandler tagHandler = new TagHandler(numThreads, runClass);
                parser.setContentHandler(tagHandler);
                parser.parse(source);
            } else {
                InputStreamReader isr = new InputStreamReader(in, "UTF-8");
                InputSource source = new InputSource(isr);
                TagHandler tagHandler = new TagHandler(numThreads, runClass);
                parser.setContentHandler(tagHandler);
                parser.parse(source);
            }


        } catch (FileNotFoundException e) {
            logger.severe("Unable to find file - I/O exception :'(");
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            logger.severe("Unable to initialize SAX parser :'(");
            e.printStackTrace();
        } catch (SAXException e) {
            logger.severe("Unable parse using SAX parser :'(");
            e.printStackTrace();
        } catch (IOException e) {
            logger.severe("Unable to find file - I/O exception :'(");
            e.printStackTrace();
        }
    }

}
