package edu.northwestern.websail.wikiparser.example.template;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.northwestern.websail.wikiparser.dumpparser.parser.Executor;
import edu.northwestern.websail.wikiparser.parsers.model.WikiTemplate;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.parser.nodes.WtTemplate;

import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class PageTemplateDriver {


    public static void main(String[] args) {
        int numThreads = 10;
        //String dumpFile = "/Users/NorThanapon/Documents/Developments/wikAPIdia/download/simple/20131114/simplewiki-20131114-pages-articles.xml";
        String dumpFileDirectory = args[0];
        boolean compressed = Boolean.valueOf(args[1]);
        String templateFilePath = args[2];
        HashMapTemplateCollector collector = new HashMapTemplateCollector();
        File folder = new File(dumpFileDirectory);
        for (File file : folder.listFiles()) {
            if (file.getName().charAt(0) == '.') continue;
            Executor.readFile(file.getAbsolutePath(), numThreads, new PageTemplateWorker(collector), compressed);
        }
        try {
            collector.serialize(templateFilePath);
        } catch (Exception e) {
            System.out.println("Serialization fails! :'(");
            e.printStackTrace();
        }
    }

    public static class HashMapTemplateCollector extends TemplateAbstractCollector {

        private HashMap<String, WikiTemplate> templateDict;

        public HashMapTemplateCollector() {
            this.templateDict = new HashMap<String, WikiTemplate>();
        }

        @Override
        public boolean exist(String templateFullTitle) {
            return this.templateDict.containsKey(templateFullTitle);
        }

        @Override
        public void set(String templateFullTitle, WikiTemplate template) {
            synchronized (this.templateDict) {
                this.templateDict.put(templateFullTitle, template);
            }
        }

        @Override
        public WikiTemplate get(String templateFullTitle) {
            return this.templateDict.get(templateFullTitle);
        }

        @Override
        public WikiTemplate get(String templateFullTitle, PageTitle wp) {
            return null;
        }

        @Override
        public void serialize(String config) throws Exception {
            Gson gson = new Gson();
            PrintStream out = new PrintStream(new GZIPOutputStream(
                    new FileOutputStream(config)));
            gson.toJson(this.templateDict, out);
            out.close();
        }

        @Override
        public TemplateAbstractCollector deserialize(String config) throws Exception {
            Gson gson = new Gson();
            InputStreamReader jsonReader = new InputStreamReader(new GZIPInputStream(new FileInputStream(config)));
            this.templateDict = gson.fromJson(jsonReader, new TypeToken<HashMap<String, WikiTemplate>>() {
            }.getType());
            return this;

        }

        @Override
        public int numTemplate() {
            return this.templateDict.size();
        }
    }

    public static class APITemplateCollector extends TemplateAbstractCollector {

        Logger log = Logger.getLogger(APITemplateCollector.class.getName());
        private HashMap<String, WikiTemplate> templateDict;

        public APITemplateCollector() {
            templateDict = new HashMap<String, WikiTemplate>();
        }

        @Override
        public boolean exist(String templateFullTitle) {
            return false;
        }

        @Override
        public void set(String templateFullTitle, WikiTemplate template) {

        }

        @Override
        public WikiTemplate get(String templateFullTitle) {
            if (templateDict.containsKey(templateFullTitle))
                return templateDict.get(templateFullTitle);

            String templateExpansion = getTemplateStrExpansion(templateFullTitle);
            WikiTemplate template = new WikiTemplate(0, templateFullTitle, templateExpansion);
            templateDict.put(templateFullTitle, template);
            return template;
        }

        @Override
        public WikiTemplate get(String templateFullTitle, PageTitle wp) {

            if (templateDict.containsKey(templateFullTitle.toLowerCase()))
                return templateDict.get(templateFullTitle.toLowerCase());

            String templateExpansion = getTemplateStrExpansion(templateFullTitle);
            WikiTemplate template = new WikiTemplate(0, templateFullTitle, templateExpansion);
            templateDict.put(templateFullTitle.toLowerCase(), template);
            return template;
        }

        @Override
        public void serialize(String config) throws Exception {

        }

        @Override
        public TemplateAbstractCollector deserialize(String config) throws Exception {
            return null;
        }

        @Override
        public int numTemplate() {
            return 0;
        }

        public String getTemplateStrExpansion(String templateFullTitle) {

            MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
            HttpClient client = new HttpClient(connectionManager);


            String templateName = templateFullTitle.split(":", 2)[1];

            if (templateName.equalsIgnoreCase(""))
                return "";
            String expansion = "";
            try {
                HttpMethod method = new GetMethod(
                        "http://en.wikipedia.org/w/api.php?action=expandtemplates&text={{"
                                + URLEncoder.encode(templateName, "UTF-8")
                                + "}}&format=json");
                log.info(method.getURI().toString());
                Integer responseCode = client.executeMethod(method);
                if (responseCode == 200) {
                    InputStream is = method.getResponseBodyAsStream();
                    StringWriter writer = new StringWriter();

                    if (is == null)
                        return "";

                    IOUtils.copy(is, writer, "UTF-8");
                    expansion = writer.toString();
                    is.close();
                    JSONObject jobj = new JSONObject(expansion);
                    JSONObject jobj2 = new JSONObject(jobj.get("expandtemplates")
                            .toString());
                    expansion = jobj2.get("*").toString();

                } else return expansion;
            } catch (Exception e) {
                System.out
                        .println("ERROR: Can't find expansion through WIKI API : "
                                + templateFullTitle + " ");
                e.printStackTrace();

            }
            return expansion;
        }

        public String getTemplateExpansionStr(WtTemplate template) {
            StringBuilder templateFullStrSb = new StringBuilder();


            return "";
        }

    }

}
