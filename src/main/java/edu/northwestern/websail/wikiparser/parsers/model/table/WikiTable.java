package edu.northwestern.websail.wikiparser.parsers.model.table;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;

/*
 * @author csbhagav
 */

public class WikiTable {

    @JsonProperty("_id")
    protected String _id;
    protected Integer pgId;
    protected Integer tableId;
    protected String pgTitle;
    protected String sectionTitle;
    protected String tableCaption;
    @Deprecated
    protected Integer wikiPgId; // Kind of obsolete. Only available from older database
    protected Integer numHeaderRows;
    protected Integer numDataRows;
    protected Integer numCols;
    protected WikiCell[][] tableData;
    protected WikiCell[][] tableHeaders;
    HashSet<Integer> numericColumns = new HashSet<Integer>();
//    HashMap<Integer, String> columnOntologyClass = new HashMap<Integer, String>();

    public WikiTable() {
    }

    @SuppressWarnings("unchecked")
    public WikiTable(Integer pgId, Integer tableId, int numHeaders,
                     int numDataRows, int numColumns) {
        super();
        this.pgId = pgId;
        this.tableId = tableId;
        this._id = this.pgId + "-" + this.tableId;
        this.tableData = new WikiCell[numDataRows][numColumns];
        this.tableHeaders = new WikiCell[numHeaders][numColumns];
        this.numHeaderRows = numHeaders;
        this.numDataRows = numDataRows;
        this.numCols = numColumns;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Integer getPgId() {
        return pgId;
    }

    public void setPgId(Integer pgId) {
        this.pgId = pgId;
    }

    public Integer getWikiPgId() {
        return wikiPgId;
    }

    public void setWikiPgId(Integer wikiPgId) {
        this.wikiPgId = wikiPgId;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public WikiCell[][] getTableData() {
        return tableData;
    }

    public void setTableData(WikiCell[][] tableData) {
        this.tableData = tableData;
    }

    public WikiCell[][] getTableHeaders() {
        return tableHeaders;
    }

    public void setTableHeaders(WikiCell[][] tableHeaders) {
        this.tableHeaders = tableHeaders;
    }

    public void setDataCell(int i, int j, WikiCell c) {
        tableData[i][j] = c;
    }

    public void setHeaderCell(int i, int j, WikiCell c) {
        tableHeaders[i][j] = c;
    }

    public String toString() {

        String retStr = "";

        for (int i = 0; i < tableData.length; i++) {
            for (int j = 0; j < tableData[i].length; j++) {

                if (!(tableData[i][j] == null))
                    retStr += tableData[i][j].toString() + "\n";
            }
            retStr = retStr.substring(0, retStr.length());
            retStr += "\n";
        }

        return retStr;

    }

    public HashSet<Integer> getNumericColumns() {
        return numericColumns;
    }

    public void setNumericColumns(HashSet<Integer> numericColumns) {
        this.numericColumns = numericColumns;
    }

    public void addNumericColumn(Integer colId) {
        numericColumns.add(colId);
    }

    public Integer getNumHeaderRows() {
        return numHeaderRows;
    }

    public void setNumHeaderRows(Integer numHeaderRows) {
        this.numHeaderRows = numHeaderRows;
    }

    public Integer getNumDataRows() {
        return numDataRows;
    }

    public void setNumDataRows(Integer numDataRows) {
        this.numDataRows = numDataRows;
    }

    public Integer getNumCols() {
        return numCols;
    }

    public void setNumCols(Integer numCols) {
        this.numCols = numCols;
    }

//    public HashMap<Integer, String> getColumnOntologyClass() {
//        return columnOntologyClass;
//    }
//
//    public void setColumnOntologyClass(
//            HashMap<Integer, String> columnOntologyClass) {
//        this.columnOntologyClass = columnOntologyClass;
//    }
//
//    public void addColumnOntologyClass(Integer id, String className) {
//        this.columnOntologyClass.put(id, className);
//    }

    public String getPgTitle() {
        return pgTitle;
    }

    public void setPgTitle(String pgTitle) {
        this.pgTitle = pgTitle;
    }

    public String getTableCaption() {
        return tableCaption;
    }

    public void setTableCaption(String tableCaption) {
        this.tableCaption = tableCaption;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }
}
