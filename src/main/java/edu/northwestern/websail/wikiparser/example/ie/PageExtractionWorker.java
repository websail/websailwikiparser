package edu.northwestern.websail.wikiparser.example.ie;

import de.fau.cs.osr.utils.FmtNotYetImplementedError;
import edu.northwestern.websail.wikiparser.dumpparser.dao.WikiPage;
import edu.northwestern.websail.wikiparser.dumpparser.parser.WikiPageAbstractWorker;
import edu.northwestern.websail.wikiparser.example.io.PageExtractionResultDelegate;
import edu.northwestern.websail.wikiparser.parsers.wikitextparser.adapter.ParsoidAdapter;
import edu.northwestern.websail.wikiparser.parsers.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.parsers.wikitextparser.sweblewrapper.SwebleWikiExtractor;
import org.sweble.wikitext.engine.EngineException;
import org.sweble.wikitext.engine.ExpansionCallback;
import org.sweble.wikitext.engine.config.WikiConfig;
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp;
import org.sweble.wikitext.parser.parser.LinkTargetException;

import java.io.IOException;
import java.util.logging.Logger;

public class PageExtractionWorker extends WikiPageAbstractWorker {
    Logger logger = Logger.getLogger(PageExtractionWorker.class.getName());
    protected static int totalParsingDone;
    protected SwebleWikiExtractor extractor;
    protected WikiExtractedPage extractedPage;
    protected WikiConfig config = DefaultConfigEnWp.generate();
    protected String langCode;
    protected PageExtractionResultDelegate extractionDelegate;


    public PageExtractionWorker(String langCode) {
        this.langCode = langCode;
        extractor = new SwebleWikiExtractor(langCode, config);
    }

    public PageExtractionWorker(String langCode, ExpansionCallback callback) {
        this.langCode = langCode;
        extractor = new SwebleWikiExtractor(langCode, config, callback);
    }

    public PageExtractionWorker(SwebleWikiExtractor extractor, String langCode) {
        this.langCode = langCode;
        this.extractor = extractor;
    }

    @Override
    public void run() {
        totalParsingDone++;
        if (totalParsingDone % 10000 == 0) {
            logger.info("Done Parsing approximately: " + totalParsingDone + " >:[");
        }
        boolean success = true;
        WikiPage page = super.getPage();
        if (this.filter(page)) return;
        String fullTitle = page.getTitle();
        int id = Integer.parseInt(page.getId());
        String wikiText = page.getRevision().getText();
        try {
            extractedPage = extractor.parse(id, fullTitle, wikiText);
            success = true;
        } catch (LinkTargetException e) {
            success = false;
        } catch (EngineException e) {
            success = false;
        } catch (IOException e) {
            success = false;
        } catch (FmtNotYetImplementedError e) {
            success = false;
        }

        if (!success) {
            logger.info("Try getting WikiText from Parsoid and parse again");
            ParsoidAdapter adapter = new ParsoidAdapter();
            String html = adapter.html(langCode + "wiki", fullTitle.replaceAll(" ", "_"));
            wikiText = adapter.wikiText(langCode + "wiki", fullTitle.replaceAll(" ", "_"), html);
            try {
                extractedPage = extractor.parse(id, fullTitle, wikiText);
                success = true;
            } catch (LinkTargetException e) {
                logger.severe(fullTitle + " is not Wikipedia full title name. It could be that the namespace is not in the system.");
                e.printStackTrace();
                success = false;
            } catch (EngineException e) {
                logger.severe("Compilation failed for " + fullTitle + " (" + id + ").");
                if (extractionDelegate != null) extractionDelegate.errorExtractingPage(id, fullTitle, wikiText);
                e.printStackTrace();
                success = false;
            } catch (IOException e) {
                logger.severe("Full page HTML Extraction failed for " + fullTitle + " (" + id + ").");
                if (extractionDelegate != null) extractionDelegate.errorExtractingPage(id, fullTitle, wikiText);
                e.printStackTrace();
                success = false;
            } catch (FmtNotYetImplementedError e) {
                logger.severe("Failed to parse due to unimplemented visit node " + fullTitle + " (" + id + ").");
                if (extractionDelegate != null) extractionDelegate.errorExtractingPage(id, fullTitle, wikiText);
                e.printStackTrace();
                success = false;
            }
        }
        if (!success) return;
        if (extractionDelegate != null) {
            extractionDelegate.doneExtractingPage(extractedPage);
        }
    }

    @Override
    public WikiPageAbstractWorker newInstance() {
        PageExtractionWorker newWorker = new PageExtractionWorker(this.extractor, this.langCode);
        newWorker.setExtractionDelegate(extractionDelegate);
        return newWorker;
    }

    protected boolean filter(WikiPage page) {
        if (page.getNamespace() == 0) {
            return page.getIsRedirect();
        } else return true;
    }

    public PageExtractionResultDelegate getExtractionDelegate() {
        return extractionDelegate;
    }

    public void setExtractionDelegate(
            PageExtractionResultDelegate extractionDelegate) {
        this.extractionDelegate = extractionDelegate;
    }


}