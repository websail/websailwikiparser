package edu.northwestern.websail.wikiparser.parsers.model.element;

public enum WikiPageLocationType {
	
	BEFORE_OVERVIEW,
	OVERVIEW,
	MAIN,
	MAIN_TABLE,
	MAIN_LIST,
    TEMPLATE,
	OTHER_TABLE
}
