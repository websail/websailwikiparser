package edu.northwestern.websail.wikiparser.wikipediaApiWrapper;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.StringWriter;

/**
 * Created by csbhagav on 1/16/14.
 */
public class WpAPI {

    HttpMethod method;
    HttpClient client;

    public WpAPI() {
        MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        client = new HttpClient(connectionManager);
    }


    public WikiApiResponsePage getWikiTextForPage(Integer pgId) {

        String wikiText = "";
        String title = "";
        try {
            method = new GetMethod(
                    "http://en.wikipedia.org/w/api.php?format=xml&action=query&pageids="
                            + pgId
                            + "&prop=revisions&rvprop=content&format=json");
            Integer responseCode = client.executeMethod(method);
            if (responseCode == 200) {
                InputStream is = method.getResponseBodyAsStream();
                StringWriter writer = new StringWriter();

                if (is == null)
                    return getWikiTextForPage(pgId);

                IOUtils.copy(is, writer, "UTF-8");
                wikiText = writer.toString();
                is.close();
                JSONObject jobj = new JSONObject(wikiText);
                JSONObject jobj2 = jobj.getJSONObject("query").getJSONObject("pages").getJSONObject(pgId.toString());
                title = jobj2.getString("title");

                wikiText = jobj2.getJSONArray("revisions").getJSONObject(0).get("*").toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        WikiApiResponsePage p = new WikiApiResponsePage();
        p.setWikiText(wikiText);
        p.setPgId(pgId);
        p.setTitle(title);
        return p;

    }


}
