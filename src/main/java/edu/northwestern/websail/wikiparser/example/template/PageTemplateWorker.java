package edu.northwestern.websail.wikiparser.example.template;

import java.util.logging.Logger;

import edu.northwestern.websail.wikiparser.dumpparser.dao.WikiPage;
import edu.northwestern.websail.wikiparser.dumpparser.parser.WikiPageAbstractWorker;
import edu.northwestern.websail.wikiparser.parsers.model.WikiTemplate;

public class PageTemplateWorker extends WikiPageAbstractWorker {
	public static Logger logger = Logger.getLogger(PageTemplateWorker.class.getName());
	TemplateAbstractCollector collector;
	protected static int totalParsingDone;
	public PageTemplateWorker(TemplateAbstractCollector collector) {
		this.collector = collector;
	}

	@Override
	public void run() {
		totalParsingDone++;
		if(totalParsingDone % 10000 == 0){
			logger.info("Done Parsing approximately: " + totalParsingDone + " >:[");
		}
		WikiPage page = super.getPage();
		if(page.getNamespace() != 10) return;
		String fullTitle = page.getTitle();
		int id = Integer.parseInt(page.getId());
		String wikiText = page.getRevision().getText();
		WikiTemplate template = new WikiTemplate(id, fullTitle, wikiText);
		if(page.getIsRedirect()){
			template.setRedirecting(true);
			template.setRedirectedTemplateName(page.getRedirectTitle());
		}
		collector.set(fullTitle, template);
	}

	@Override
	public WikiPageAbstractWorker newInstance() {
		return new PageTemplateWorker(collector);
	}

}
