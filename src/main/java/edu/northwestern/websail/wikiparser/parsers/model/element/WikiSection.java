package edu.northwestern.websail.wikiparser.parsers.model.element;

public class WikiSection extends WikiElement {

    private int level;
    private int endHeadingOffset;
    private int sectionNum;
    private String sectionTitle;

    public WikiSection() {
        super();
    }

    public WikiSection(int start, int end, WikiPageLocationType locType, int level, int endHeading, int sectionNum) {
        super(start, end, locType);
        this.setLevel(level);
        this.setEndHeadingOffset(endHeading);
        this.setSectionNum(sectionNum);
    }

    public WikiSection(int start, int end, WikiPageLocationType locType, boolean isInTemplate, int level, int endHeading, int sectionNum) {
        super(start, end, locType, isInTemplate);
        this.setLevel(level);
        this.setEndHeadingOffset(endHeading);
        this.setSectionNum(sectionNum);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getEndHeadingOffset() {
        return endHeadingOffset;
    }

    public void setEndHeadingOffset(int endHeadingOffset) {
        this.endHeadingOffset = endHeadingOffset;
    }

    public int getSectionNum() {
        return sectionNum;
    }

    public void setSectionNum(int sectionNum) {
        this.sectionNum = sectionNum;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }
}
