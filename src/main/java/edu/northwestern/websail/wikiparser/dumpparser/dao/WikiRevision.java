package edu.northwestern.websail.wikiparser.dumpparser.dao;

/**
 * 
 * @author csbhagav
 * 
 *         This class holds information contained in the "revision" node of
 *         Wikipedia XML dump
 */

public class WikiRevision {

	String id;
	String timeStamps;
	WikiContributor contributor;
	String comment;
	String text;

	public WikiRevision(String id, String timeStamps,
			WikiContributor contributor, String comment, String text) {
		super();
		this.id = id;
		this.timeStamps = timeStamps;
		this.contributor = contributor;
		this.comment = comment;
		this.text = text;
	}

	public WikiRevision() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTimeStamps() {
		return timeStamps;
	}

	public void setTimeStamps(String timeStamps) {
		this.timeStamps = timeStamps;
	}

	public WikiContributor getContributor() {
		return contributor;
	}

	public void setContributor(WikiContributor contributor) {
		this.contributor = contributor;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String toString() {
		String retStr = "\n";
		retStr += "\tID: " + id + "\n";
		retStr += "\tTimestamp: " + timeStamps + "\n";
		retStr += "\tComment: " + comment + "\n";
		retStr += "\tText: " + text + "\n";
		retStr += "\tContributor:\n " + contributor.toString() + "\n";

		return retStr;
	}
}
