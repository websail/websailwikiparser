package edu.northwestern.websail.wikiparser.parsers.model.element;

public class WikiHTMLElement extends WikiElement {

    protected String html;
    protected Integer inSectionNumber;

    public WikiHTMLElement() {
        super();
    }

    public WikiHTMLElement(int start, int end, WikiPageLocationType locType, String html,
                           boolean isInTemplate) {
        super(start, end, locType, isInTemplate);
        this.html = html;
    }

    public WikiHTMLElement(int start, int end, WikiPageLocationType locType, String html) {
        super(start, end, locType);
        this.html = html;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String toString() {
        return html;
    }

    public Integer getInSectionNumber() {
        return inSectionNumber;
    }

    public void setInSectionNumber(Integer inSectionNumber) {
        this.inSectionNumber = inSectionNumber;
    }
}
