package edu.northwestern.websail.wikiparser.example;

import edu.northwestern.websail.wikiparser.dumpparser.parser.WikiPageAbstractWorker;
import edu.northwestern.websail.wikiparser.example.titleprinter.PageTitleWorker;



/**
 * 
 * @author csbhagav
 * 
 *         Example class that extends {@link PageTitleWorker}
 */
public class Test extends WikiPageAbstractWorker {

	public void run() {

		System.out.println(getPage().getId());

	}

	@Override
	public WikiPageAbstractWorker newInstance() {
		return new Test();
	}

}
