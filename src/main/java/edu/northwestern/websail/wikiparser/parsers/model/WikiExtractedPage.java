package edu.northwestern.websail.wikiparser.parsers.model;

import edu.northwestern.websail.wikiparser.parsers.model.element.WikiHTMLElement;
import edu.northwestern.websail.wikiparser.parsers.model.element.WikiLink;
import edu.northwestern.websail.wikiparser.parsers.model.element.WikiParagraph;
import edu.northwestern.websail.wikiparser.parsers.model.element.WikiSection;
import org.jongo.marshall.jackson.oid.ObjectId;

import java.util.ArrayList;

public class WikiExtractedPage {
	@ObjectId
	protected String key; //for jongo
	protected String plainText;
	protected ArrayList<WikiSection> sections;
	protected ArrayList<WikiParagraph> paragraphs;
	protected ArrayList<WikiLink> internalLinks;
	protected ArrayList<WikiLink> interWikiLinks;
	protected ArrayList<WikiLink> categoryLinks;
	protected ArrayList<WikiHTMLElement> tableHTMLs;
	protected WikiTitle title;
	
	
	public WikiExtractedPage(){
		
	}
	public WikiExtractedPage (WikiTitle title){
		this.title=title;
	}
	public WikiTitle getTitle() {
		return title;
	}

	public void setTitle(WikiTitle title) {
		this.title = title;
	}
	public String getPlainText() {
		return plainText;
	}
	public ArrayList<WikiSection> getSections() {
		return sections;
	}
	public ArrayList<WikiParagraph> getParagraphs() {
		return paragraphs;
	}
	public ArrayList<WikiLink> getInternalLinks() {
		return internalLinks;
	}
	public ArrayList<WikiLink> getInterWikiLinks() {
		return interWikiLinks;
	}
	public ArrayList<WikiLink> getCategoryLinks() {
		return categoryLinks;
	}
	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}
	public void setSections(ArrayList<WikiSection> sections) {
		this.sections = sections;
	}
	public void setParagraphs(ArrayList<WikiParagraph> paragraphs) {
		this.paragraphs = paragraphs;
	}
	public void setInternalLinks(ArrayList<WikiLink> internalLinks) {
		this.internalLinks = internalLinks;
	}
	public void setInterWikiLinks(ArrayList<WikiLink> interWikiLinks) {
		this.interWikiLinks = interWikiLinks;
	}
	public void setCategoryLinks(ArrayList<WikiLink> categoryLinks) {
		this.categoryLinks = categoryLinks;
	}
	public ArrayList<WikiHTMLElement> getTableHTMLs() {
		return tableHTMLs;
	}
	public void setTableHTMLs(ArrayList<WikiHTMLElement> tableHTMLs) {
		this.tableHTMLs = tableHTMLs;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String _id) {
		this.key = _id;
	}
	
	
	
}
