package edu.northwestern.websail.wikiparser.example.titleprinter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import edu.northwestern.websail.wikiparser.dumpparser.parser.Executor;


public class PageTitlePrinter {

	/**
	 * @author csbhagav
	 * 
	 *         This class allows command line execution of the program. This is
	 *         only for test purposes. Not a part of the Library
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		CommandLineParser cp = new GnuParser();
		HelpFormatter formatter = new HelpFormatter();
		Options opts = getOptions();
		CommandLine cl;
		String dumpFile = "";
		try {
			cl = cp.parse(opts, args);
			Integer numThreads = cl.getOptionValue("t") == null ? 1 : Integer
					.valueOf(cl.getOptionValue("t"));
			dumpFile = cl.getOptionValue("input");
			dumpFile = "/home/csbhagav/ling/data/simplewiki-20130525-pages-articles.xml";
			Executor.readFile(dumpFile, numThreads, new PageTitleWorker(0));

		} catch (ParseException e) {
			System.out.println("Unable to parse input arguments");

			formatter.printHelp("java -jar wikiDumpParse.jar", opts);
		}

	}

	private static Options getOptions() {

		Options opts = new Options();
		Option threadOpt = new Option("t", "numThreads", true,
				"Number of threads to be used for parsing. Default = 1");
		threadOpt.setRequired(false);
		opts.addOption(threadOpt);

		Option fileOpt = new Option("i", "input", true,
				"XML dump file to be parsed");
		fileOpt.setRequired(true);
		opts.addOption(fileOpt);

		return opts;

	}

}
